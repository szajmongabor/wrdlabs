import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs';
import { PollType } from 'src/app/enums/poll.enum';
import { PollAnswer } from 'src/app/models/poll-answer.model';
import { PollService } from 'src/app/services/poll.service';

@Component({
  selector: 'app-poll',
  templateUrl: './poll.component.html',
  styleUrls: ['./poll.component.scss']
})
export class PollComponent implements OnInit {

  isSummary = false;

  polls?: PollAnswer[];

  pollType = PollType;

  selectedPollId = 0;

  constructor(private _pollService: PollService, private _route: ActivatedRoute, private _router: Router) { }

  isPreviousButtonDisabled(): boolean {
    return (this.selectedPollId === 0);
  }

  isNextButtonDisplayed(): boolean {
    return (this.selectedPollId !== this.polls!.length - 1);
  }

  ngOnInit(): void {
    this._pollService.getPollConfig()
      .pipe(take(1))
      .subscribe(polls => {
        this.polls = polls.map(poll => ({...poll, userAnswer: ''})) as PollAnswer[];
        this._route.paramMap
          .subscribe(paramMap => {
            const id = paramMap.get('id');
            if (id && !isNaN(+id) && this.polls!.length >= +id) {
              this.selectedPollId = +id;
            } else {
              console.log('not that');
              this._router.navigate(['/poll/0']);
            }
          });
      });
  }

  onActionButtonClicked(isLeft: boolean): void {
    // TODO message if filled
    if (this.polls![this.selectedPollId].userAnswer || isLeft) {
      this.selectedPollId = isLeft ? this.selectedPollId - 1 : (this.isNextButtonDisplayed() ? this.selectedPollId + 1 : this.polls!.length);
      this.isSummary = this.selectedPollId === this.polls!.length;
      this._router.navigate([`/poll/${this.isSummary ? 'summary' : this.selectedPollId}`]);
    }
  }

  onSelectedValueChanged(event: Event): void {
    const value = (event.target as HTMLInputElement).value;
    if (this.polls![this.selectedPollId].type === PollType.SelectMore) {
      this.polls![this.selectedPollId].userAnswer += this.polls![this.selectedPollId].userAnswer.length ? `, ${value}` : value;
    } else {
      this.polls![this.selectedPollId].userAnswer = value;
    }
  }

}
