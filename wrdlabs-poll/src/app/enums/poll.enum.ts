export enum PollType {
  SelectOne = 'SELECT_ONE',
  SelectMore = 'SELECT_MORE',
  FreeText = 'FREE_TEXT'
}
