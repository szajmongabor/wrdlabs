import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Poll } from '../interfaces/poll.interface';

@Injectable({
  providedIn: 'root'
})
export class PollService {

  constructor(private _http: HttpClient) { }

  getPollConfig(): Observable<Poll[]> {
    return this._http.get<Poll[]>('./../assets/config/poll.json');
  }
}
