import { PollType } from "../enums/poll.enum";

export interface Poll {
  question: string;
  type: PollType;
  answers?: string[];
}
