import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PollComponent } from './components/poll/poll.component';

const routes: Routes = [
  { path: 'poll/:id', component: PollComponent },
  { path: '**', redirectTo: 'poll/0' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
