import { PollType } from "../enums/poll.enum";
import { Poll } from "../interfaces/poll.interface";

export class PollAnswer implements Poll {
  question = '';
  type = PollType.SelectOne;
  answers = [];
  userAnswer = '';
}
